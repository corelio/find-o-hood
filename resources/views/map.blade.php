<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="A mongoDB example for real state industry - Let's find out what is the desired neighborhood ">
    <meta name="author" content="Corelio">

    <title>FindOhood</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
        <li class="sidebar-brand">
            <a href="#top" onclick=$("#menu-close").click();>Find-O-Hood<i class="fa fa-copyright"></i></a>
        </li>
        <li>
            <a href="#top" onclick=$("#menu-close").click();>Home</a>
        </li>
        <li>
            <a href="#map" onclick=$("#menu-close").click();>Find</a>
        </li>
        <li>
            <a href="#about" onclick=$("#menu-close").click();>About</a>
        </li>
    </ul>
</nav>

<!-- Header -->
<header id="top" class="header">
    <div class="text-vertical-center">
        <h1>Find-O-Hood<sup><i class="fa fa-copyright fa-fw"></i></sup></h1>
        <h3>Everything you ever wished for, one click away</h3>
        <h2><i>Find-O-Hood<sup><i class="fa fa-copyright fa-fw"></i></sup></i> will help you find your dream house, close to what is important to you</h2>
        <br>
        <a href="#map" class="btn btn-dark btn-lg">Try me</a>
    </div>
</header>

<!-- Map -->
<section id="map" class="map">
    <div class="col-md-12 inventory">
        <div style="width: 100%; height: 580px" id="findohood"></div>
        <script>
            function initMap() {
                var myLatlng = {
                    @if(isset($near))
                        lat: {{ $near['lat'] }},
                        lng: {{ $near['lng'] }}
                    @else
                        lat: 45.55,
                        lng: -73.64
                    @endif
                };

                var you = {
                    url: '/favicon.ico',
                };
                var other = {
                    url: '/service.ico',
                    labelOrigin: new google.maps.Point(20,40)
                };

                var map = new google.maps.Map(document.getElementById('findohood'), {
                    zoom: 14,
                    center: {
                        lat: 45.55,
                        lng: -73.64
                    }
                });

                var markersArray = [];

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    icon: you,
                    map: map,
                });

                markersArray.push(marker);

                        @if(isset($near))
                        @foreach($near['services'] as $distance => $service)
                var markerOther = new google.maps.Marker({
                            position: {lat: {{ $service[0]->location[1] }}, lng: {{ $service[0]->location[0] }}},
                            icon: other,
                            map: map,
                            label: {
                                text: '{{ $service[0]->name }}' + ' [' + '{{ $distance/100 }}' + ' km]',
                                color: 'black'
                            }
                        });
                markersArray.push(markerOther);
                @endforeach
               @endif

               map.addListener('click', function (event) {
                    if(markersArray.length > 1)
                    {
                        for(var i = 1; i < markersArray.length; i++)
                        {
                            markersArray[i].setMap(null);
                        }
                        map.setZoom(14);
                    }
                    marker.setPosition(event.latLng);
                    document.getElementById('lat').value = event.latLng.lat();
                    document.getElementById('long').value = event.latLng.lng();
                    document.getElementById('findohood-button').disabled = false;
                    document.getElementById('findohood-button').value = "Ready? So click me";
                    document.getElementById('findohood-button').classList.remove('btn-danger');
                    document.getElementById('findohood-button').classList.add('btn-success');
                });

                var boundariesCoordinates = [
                    {lat: 45.565, lng: -73.685},
                    {lat: 45.536, lng: -73.685},
                    {lat: 45.536, lng: -73.588},
                    {lat: 45.565, lng: -73.588},
                    {lat: 45.565, lng: -73.685}
                ];

                var boundariesPath = new google.maps.Polyline({
                    path: boundariesCoordinates,
                    geodesic: true,
                    strokeColor: 'black',
                    strokeOpacity: 0.3,
                    strokeWeight: 2
                });

                boundariesPath.setMap(map);

            }
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzy4xKW78CUxjzP12Kv4V_ywAXS3pHbT0&callback=initMap">
        </script>
        <div class="form-button">
            <form action="{{ url('/#map') }}" method="post" id="runOhood">
                {{ csrf_field() }}
                <input type="hidden" name="lat" id="lat" />
                <input type="hidden" name="long" id="long" />
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1 text-center">
                        <input disabled="disabled" class="btn btn-danger" type="submit" name="button" id="findohood-button" value="Select a place inside de gray area" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<!-- Footer -->
<footer>
    <div id="about" class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 text-center">
                <h4><strong>FindOhood</strong>
                </h4>
                <span><small>A project for the REOL interview</small></span>
                <p>Marco Corsini
                <ul class="list-unstyled">
                    <li><i class="fa fa-phone fa-fw"></i> +55 11 98141 2698</li>
                    <li><i class="fa fa-envelope-o fa-fw"></i> <a href="mailto:corsini@gmail.com">corsini@gmail.com</a>
                    </li>
                </ul>
                <br>
                <ul class="list-inline">
                    <li>
                        <a href="https://www.facebook.com/marco.a.corsini"><i class="fa fa-facebook fa-fw fa-3x"></i></a>
                    </li>
                    <li>
                        <a href="https://twitter.com/corelio"><i class="fa fa-twitter fa-fw fa-3x"></i></a>
                    </li>
                </ul>
                <hr class="small">
                <p class="text-muted">Copyright &copy; FindOhood 2017</p>
            </div>
        </div>
    </div>
    <a id="to-top" href="#top" class="btn btn-dark btn-lg"><i class="fa fa-chevron-up fa-fw fa-1x"></i></a>
</footer>

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Custom Theme JavaScript -->
<script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    // Scrolls to the selected menu item on the page
    $(function() {
        $('a[href*=#]:not([href=#],[data-toggle],[data-target],[data-slide])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
    //#to-top button appears after scrolling
    var fixed = false;
    $(document).scroll(function() {
        if ($(this).scrollTop() > 250) {
            if (!fixed) {
                fixed = true;
                // $('#to-top').css({position:'fixed', display:'block'});
                $('#to-top').show("slow", function() {
                    $('#to-top').css({
                        position: 'fixed',
                        display: 'block'
                    });
                });
            }
        } else {
            if (fixed) {
                fixed = false;
                $('#to-top').hide("slow", function() {
                    $('#to-top').css({
                        display: 'none'
                    });
                });
            }
        }
    });
    // Disable Google Maps scrolling
    // See http://stackoverflow.com/a/25904582/1607849
    // Disable scroll zooming and bind back the click event
    var onMapMouseleaveHandler = function(event) {
        var that = $(this);
        that.on('click', onMapClickHandler);
        that.off('mouseleave', onMapMouseleaveHandler);
        that.find('iframe').css("pointer-events", "none");
    }
    var onMapClickHandler = function(event) {
        var that = $(this);
        // Disable the click handler until the user leaves the map area
        that.off('click', onMapClickHandler);
        // Enable scrolling zoom
        that.find('iframe').css("pointer-events", "auto");
        // Handle the mouse leave event
        that.on('mouseleave', onMapMouseleaveHandler);
    }
    // Enable map zooming with mouse scroll when the user clicks the map
    $('.map').on('click', onMapClickHandler);
</script>

</body>

</html>