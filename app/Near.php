<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Near extends Eloquent
{

    protected $connection = 'mongodb';

    protected $collection = 'services';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        '_id', 'name', 'location'
    ];


}
