<?php

namespace App\Http\Controllers;

use App\Traits\Distance;
use Illuminate\Http\Request;
use App\Near;

class NearController extends Controller
{
    use Distance;

    /**
     * Collect all services around the position given
     * Return array using the distance as keys
     * Distance is rounded
     *
     * @param Request $request
     * @return array
     */
    public function checkAround(Request $request)
    {

        $long = $request->input('long');
        $lat = $request->input('lat');

        $query = ['location' => [
            '$nearSphere' => [
                '$geometry' => [
                    'type' => 'Point',
                    'coordinates' => [
                        (floatval($long)),
                        (floatval($lat))
                    ]
                ],
                '$maxDistance' => 1400 //Search around 2 km
            ]
        ]];

        $near = Near::whereRaw($query)->get(['name', 'location']);
        $result = [];
        foreach($near as $data)
        {
            $result[$this->distance(floatval($long), floatval($lat), $data->location[0], $data->location[1], 'k')*100][] = $data;
        }

        return view('map')->with([
            'near' => [
                'lat' => $lat,
                'lng' => $long,
                'services' => $result
            ]
        ]);

    }
}
