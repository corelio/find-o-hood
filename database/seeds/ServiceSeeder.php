<?php

use Illuminate\Database\Seeder;
use App\Near;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $services = 100;

        for ($i = 0; $i < $services; $i++) {
            $service = new Near();
            $service->name = $faker->company;
            $service->location = [$faker->longitude(-73.588, -73.685), $faker->latitude(45.536, 45.565)];
            $service->save();
        }
    }
}
